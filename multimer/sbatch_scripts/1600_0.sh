#!/bin/bash
#SBATCH -A berzelius-2023-295
#SBATCH --gpus 1
#SBATCH -t 3-00:00:00
#SBATCH -C thin
#SBATCH -o /proj/berzelius-2021-29/users/x_arnel/herpes/multimer/logs/1600_0.log

export TF_FORCE_UNIFIED_MEMORY='1'
export XLA_PYTHON_CLIENT_MEM_FRACTION='6.0'

module load Anaconda/2021.05-nsc1

conda activate /proj/beyondfold/apps/.conda/envs/af_server 
 python /proj/beyondfold/apps/alphafoldv2.3.1_cache/run_alphafold.py --flagfile /proj/beyondfold/users/x_clami/mmseqs_benchmark/scripts/multimer_all_vs_all.flag --output_dir /proj/berzelius-2021-29/users/x_arnel/herpes/multimer --fasta_paths /proj/berzelius-2021-29/users/x_arnel/herpes/multimer/P60709_P04296/P60709_P04296.fasta,/proj/berzelius-2021-29/users/x_arnel/herpes/multimer/P07384_P10231/P07384_P10231.fasta,/proj/berzelius-2021-29/users/x_arnel/herpes/multimer/Q9P2E9_P04487/Q9P2E9_P04487.fasta,/proj/berzelius-2021-29/users/x_arnel/herpes/multimer/P27824_P10211/P27824_P10211.fasta,/proj/berzelius-2021-29/users/x_arnel/herpes/multimer/P05023_P04488/P05023_P04488.fasta,/proj/berzelius-2021-29/users/x_arnel/herpes/multimer/Q69091_Q92835/Q69091_Q92835.fasta,/proj/berzelius-2021-29/users/x_arnel/herpes/multimer/P06491_P10599/P06491_P10599.fasta,/proj/berzelius-2021-29/users/x_arnel/herpes/multimer/Q00341_P04487/Q00341_P04487.fasta,/proj/berzelius-2021-29/users/x_arnel/herpes/multimer/P08392_P35268/P08392_P35268.fasta,/proj/berzelius-2021-29/users/x_arnel/herpes/multimer/P10211_P11021/P10211_P11021.fasta,/proj/berzelius-2021-29/users/x_arnel/herpes/multimer/P10211_P04844/P10211_P04844.fasta,/proj/berzelius-2021-29/users/x_arnel/herpes/multimer/P10211_P07237/P10211_P07237.fasta,/proj/berzelius-2021-29/users/x_arnel/herpes/multimer/P10211_P08195/P10211_P08195.fasta,/proj/berzelius-2021-29/users/x_arnel/herpes/multimer/P10211_P21589/P10211_P21589.fasta,/proj/berzelius-2021-29/users/x_arnel/herpes/multimer/P10211_P13667/P10211_P13667.fasta,/proj/berzelius-2021-29/users/x_arnel/herpes/multimer/P10211_P30101/P10211_P30101.fasta,/proj/berzelius-2021-29/users/x_arnel/herpes/multimer/P10211_Q99805/P10211_Q99805.fasta --pickle_cache cache/ --pad_to_size 1600,20000 
