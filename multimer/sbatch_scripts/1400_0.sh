#!/bin/bash
#SBATCH -A berzelius-2023-295
#SBATCH --gpus 1
#SBATCH -t 3-00:00:00
#SBATCH -C thin
#SBATCH -o /proj/berzelius-2021-29/users/x_arnel/herpes/multimer/logs/1400_0.log

export TF_FORCE_UNIFIED_MEMORY='1'
export XLA_PYTHON_CLIENT_MEM_FRACTION='6.0'

module load Anaconda/2021.05-nsc1

conda activate /proj/beyondfold/apps/.conda/envs/af_server 
 python /proj/beyondfold/apps/alphafoldv2.3.1_cache/run_alphafold.py --flagfile /proj/beyondfold/users/x_clami/mmseqs_benchmark/scripts/multimer_all_vs_all.flag --output_dir /proj/berzelius-2021-29/users/x_arnel/herpes/multimer --fasta_paths /proj/berzelius-2021-29/users/x_arnel/herpes/multimer/O43852_P10211/O43852_P10211.fasta,/proj/berzelius-2021-29/users/x_arnel/herpes/multimer/Q00839_P10226/Q00839_P10226.fasta,/proj/berzelius-2021-29/users/x_arnel/herpes/multimer/P08133_P04488/P08133_P04488.fasta,/proj/berzelius-2021-29/users/x_arnel/herpes/multimer/Q1KMD3_P10226/Q1KMD3_P10226.fasta,/proj/berzelius-2021-29/users/x_arnel/herpes/multimer/Q9BS26_P10211/Q9BS26_P10211.fasta,/proj/berzelius-2021-29/users/x_arnel/herpes/multimer/P04294_P13010/P04294_P13010.fasta,/proj/berzelius-2021-29/users/x_arnel/herpes/multimer/P10211_Q9BTV4/P10211_Q9BTV4.fasta,/proj/berzelius-2021-29/users/x_arnel/herpes/multimer/P10211_P01889/P10211_P01889.fasta,/proj/berzelius-2021-29/users/x_arnel/herpes/multimer/P10211_Q12907/P10211_Q12907.fasta,/proj/berzelius-2021-29/users/x_arnel/herpes/multimer/P10211_Q15293/P10211_Q15293.fasta,/proj/berzelius-2021-29/users/x_arnel/herpes/multimer/P10211_P10321/P10211_P10321.fasta,/proj/berzelius-2021-29/users/x_arnel/herpes/multimer/P10211_P50454/P10211_P50454.fasta,/proj/berzelius-2021-29/users/x_arnel/herpes/multimer/P10211_Q15084/P10211_Q15084.fasta,/proj/berzelius-2021-29/users/x_arnel/herpes/multimer/Q9HDC9_P10211/Q9HDC9_P10211.fasta,/proj/berzelius-2021-29/users/x_arnel/herpes/multimer/P07737_P08543/P07737_P08543.fasta,/proj/berzelius-2021-29/users/x_arnel/herpes/multimer/P27797_P10211/P27797_P10211.fasta,/proj/berzelius-2021-29/users/x_arnel/herpes/multimer/P04296_P07737/P04296_P07737.fasta,/proj/berzelius-2021-29/users/x_arnel/herpes/multimer/P16070_P10228/P16070_P10228.fasta --pickle_cache cache/ --pad_to_size 1400,20000 
