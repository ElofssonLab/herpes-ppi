import re
import glob
import pickle
import string
import base64
import hashlib
import argparse
from sys import argv
from pathlib import Path
from Bio import AlignIO
import pandas as pd

parser = argparse.ArgumentParser(description="Converts an a3m alignment (e.g. MMseqs2 alignment) to a Stockholm alignment \
                                 that can be used for MSA pairing in AlphaFold-multimer runs")
parser.add_argument("in_alignment", help = "Path to the a3m alignment file")
parser.add_argument("out_dir", help = "Path to output directory")
parser.add_argument("colab_db_path", help = "Path to ColabFold databases")
parser.add_argument("--taxid", action="store_true", default=True, help = "If tax IDs should be used in the pairing procedure")
parser.add_argument("--repid", action="store_true", default=False, help = "If mnemonic species IDs should be used in the pairing procedure")
args = parser.parse_args()

seqids = set()

Path(args.out_dir).mkdir(parents=True, exist_ok=True)
tolower = str.maketrans('', '', string.ascii_lowercase)
table = str.maketrans('', '', string.ascii_lowercase)

seqid_to_taxid = Path(args.colab_db_path, "seqid2taxid.pkl")
seqid_to_repid = Path(args.colab_db_path, "seqid2repid.pkl")

if seqid_to_taxid.exists() and seqid_to_repid.exists():
    if args.taxid:
        with open(seqid_to_taxid, "rb") as input_file:
            seqid_to_taxid_dic = pickle.load(input_file)        
    else:
        with open(seqid_to_repid, "rb") as input_file:
            seqid_to_repid_dic = pickle.load(input_file)
else: # generate it from the colabfold DB
    print("Generating accession2taxid pickle file")
    seqid_to_taxid_dic = {}
    seqid_to_repid_dic = {}
    header_files = glob.glob(f"{args.colab_db_path}/*_h.tsv")
    ptax = re.compile("TaxID=([0-9]+)")
    prep = re.compile("RepID=[A-Z0-9]+[_]([A-Z0-9]+)")
    
    for header_file in header_files:
        print(f"Looking for taxids in {header_file}")
        with open(header_file) as headers:
            for header in headers:
                match_taxid = ptax.search(header)
                match_repid = prep.search(header)
                accession = header.split()[1]
                if match_taxid:
                    taxid = match_taxid.group(1)
                    seqid_to_taxid_dic[accession] = int(taxid)
                if match_repid:
                    repid = match_repid.group(1)
                    seqid_to_repid_dic[accession] = repid
    
    with open(seqid_to_taxid, 'wb') as f:
      pickle.dump(seqid_to_taxid_dic, f, protocol=4)
    with open(seqid_to_repid, 'wb') as f:
      pickle.dump(seqid_to_repid_dic, f, protocol=4)

pseudo_uniprot = open(f"{args.out_dir}/uniprot_hits.a3m", "w")

has_taxid = False

with open(args.in_alignment) as aln:
    a3m_data = iter(aln.readlines())

# always write first (target) sequence to file
pseudo_uniprot.write(next(a3m_data))
target = next(a3m_data)
pseudo_uniprot.write(target)

for line in a3m_data:
    if line.startswith(">"):
        memid = None
        seqid = line.split()[0].strip(">") # gets accession ID
        print(seqid)
        if seqid not in seqids: # avoids duplicates
            seqids.add(seqid)
        else:
            continue
        
        if args.repid and seqid in seqid_to_repid_dic:
            memid = seqid_to_repid_dic[seqid]
        elif args.taxid and seqid in seqid_to_taxid_dic:
            taxid = seqid_to_taxid_dic[seqid]
            memid = base64.urlsafe_b64encode(hashlib.md5(str(taxid).encode('utf-8')).digest()).decode("utf-8").replace("_", "").replace("-", "")[:5].upper()
            
        if memid:
            seqid_alpha = re.sub(r"[^a-zA-Z0-9]", '', seqid)
            pseudo_uniprot.write(f">tr|{seqid_alpha}|{seqid_alpha}_{memid}/1-{len(target)}\n")

    elif memid:
            pseudo_uniprot.write(line.translate(tolower))

pseudo_uniprot.close()

input_handle  = open(f"{args.out_dir}/uniprot_hits.a3m", "r")
output_handle = open(f"{args.out_dir}/uniprot_hits.sto", "w")

alignments = AlignIO.parse(input_handle, "fasta")
AlignIO.write(alignments, output_handle, "stockholm")

output_handle.close()
input_handle.close()


if __name__ == '__main__':
  main()
