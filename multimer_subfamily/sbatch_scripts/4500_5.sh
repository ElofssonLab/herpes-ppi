#!/bin/bash
#SBATCH -A berzelius-2023-295
#SBATCH --gpus 1
#SBATCH -t 3-00:00:00
#SBATCH -C thin
#SBATCH -o /proj/berzelius-2021-29/users/x_arnel/herpes/multimer_subfamily/logs/4500_5.log

export TF_FORCE_UNIFIED_MEMORY='1'
export XLA_PYTHON_CLIENT_MEM_FRACTION='6.0'

module load Anaconda/2021.05-nsc1

conda activate /proj/beyondfold/apps/.conda/envs/af_server 
 python /proj/beyondfold/apps/alphafoldv2.3.1_cache/run_alphafold.py --flagfile /proj/beyondfold/users/x_clami/mmseqs_benchmark/scripts/multimer_all_vs_all.flag --output_dir /proj/berzelius-2021-29/users/x_arnel/herpes/multimer_subfamily --fasta_paths /proj/berzelius-2021-29/users/x_arnel/herpes/multimer_subfamily/P10211_P14625/P10211_P14625.fasta --pickle_cache cache/subfamily/ --pad_to_size 1707,20000 
